import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDao {
	
	//Dependency Injection for ProductRepository
	@Autowired
	ProductRepository productRepo;

	public List<Product> getAllProducts() {		
		List<Product> productList = productRepo.findAll();		
		return productList;
	}
	public List<Product> getProductByName(String prodName) {
		List<Product> productList = productRepo.findByName(prodName);	
		return productList;
	}

	public Product addProduct(Product product) {
		Product prod = productRepo.save(product);
		return prod;
	}
	

	
}