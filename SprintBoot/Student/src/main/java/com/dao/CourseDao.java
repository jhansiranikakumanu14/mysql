package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Course;

@Service 
public class CourseDao {
	@Autowired
	CourseRepository courseRepository;

	public  List<Course> getAllCourses() {
		return courseRepository.findAll();
	}

	public Course getCourseById(int cousrseId) {
		return courseRepository.findById(cousrseId).orElse(null);
	}

	public Course getCourseByName(String deptName) {
		return courseRepository.findByName(deptName);
	}

	public Course addCourse(Course courses) {
		return courseRepository.save(courses);
	}

	public Course updateCourse(Course courses) {
		return courseRepository.save(courses);
	}

	public void deleteCourseById(int deptId) {
		courseRepository.deleteById(deptId);
	}

}


