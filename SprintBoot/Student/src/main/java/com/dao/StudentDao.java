package com.dao;

	import java.util.List;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Service;

	import com.model.Student;

	@Service
	public class StudentDao {
		
		//Dependency Injection for ProductRepository
		@Autowired
		StudentRepository studentRepo;
 
		public List<Student> getAllStudents() {		
			List<Student> studentList = studentRepo.findAll();		
			return studentList;
		}
		public Student getStudentById(int studId) {
			Student demoStudent = new Student(0, "Student Not Found!", 0, null, null, 0.0, null, null);
			Student student = studentRepo.findById(studId).orElse(demoStudent);
			return student;
		}
		public List<Student> getStudentByName(String studName) {
			List<Student> studentList = studentRepo.findByName(studName);	
			return studentList;
		}
		public List<Student> getStudentByCourse(String studCourse) {
			List<Student> studentList = studentRepo.findByCourse(studCourse);	
			return studentList;
		}
		public Student addStudent(Student student) {
			Student stud = studentRepo.save(student);
			return stud;
		}
		public Student updateStudent(Student student) {
			Student stud = studentRepo.save(student);
			return stud;
		}
		public void deleteStudentById(int studId) {
			studentRepo.deleteById(studId);
		}
		public List<Student> StudentByEmailIdAndPassword(String studEmailId, String studPassword) {
			List<Student> studentList = studentRepo.findByEmailAndPassword(studEmailId,studPassword);
			return studentList;
		}
		public List<Student> StudentLogin(String studEmailId, String studPassword) {
			List<Student> studentList = studentRepo.StudentLogin(studEmailId, studPassword);
			return studentList;
	    }
		
	}

