package day02;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class Demo06 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter EmployeeID to retrieve the record:");
		int empId = scanner.nextInt();
		System.out.println();

		String selectQuery = "SELECT * FROM employee WHERE empId=?";

		try {
			preparedStatement = connection.prepareStatement(selectQuery);

			preparedStatement.setInt(1, empId);

			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				int employeeId = resultSet.getInt("empId");
				String name = resultSet.getString("empName");
				double salary = resultSet.getDouble("salary");
				String gender = resultSet.getString("gender");
				String email = resultSet.getString("emailId");
				String password = resultSet.getString("password");

				System.out.println("EmployeeID: " + employeeId);
				System.out.println("Name: " + name);
				System.out.println("Salary: " + salary);
				System.out.println("Gender: " + gender);
				System.out.println("Email: " + email);
				System.out.println("Password: " + password);
			} else {
				System.out.println("No record found for EmployeeID: " + empId);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}
				if (preparedStatement != null) {
					preparedStatement.close();
				}
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
