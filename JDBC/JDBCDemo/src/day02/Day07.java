package day02;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;

public class Day07 {
	public static void main(String[] args) {
        Connection connection = DbConnection.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String selectQuery = "SELECT * FROM employee";

        try {
            preparedStatement = connection.prepareStatement(selectQuery);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int employeeId = resultSet.getInt("empId");
                String name = resultSet.getString("empName");
                double salary = resultSet.getDouble("salary");
                String gender = resultSet.getString("gender");
                String email = resultSet.getString("emailId");
                String password = resultSet.getString("password");

                System.out.println("EmployeeID: " + employeeId);
                System.out.println("Name: " + name);
                System.out.println("Salary: " + salary);
                System.out.println("Gender: " + gender);
                System.out.println("Email: " + email);
                System.out.println("Password: " + password);
                System.out.println("  ");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
