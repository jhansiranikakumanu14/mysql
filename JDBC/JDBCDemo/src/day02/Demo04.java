package day02;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

//Update emp record
public class Demo04 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter EmployeeID and Salary");
		int empId = scanner.nextInt();
		double salary = scanner.nextDouble();
		System.out.println();
		
		String insertQuery = "Update employee set salary=? where empId=?";
		
		try {
			preparedStatement = connection.prepareStatement(insertQuery);
						
			preparedStatement.setDouble(1, salary);
			preparedStatement.setInt(2, empId);
			
			int result = preparedStatement.executeUpdate();
			
			if (result > 0) {
				System.out.println("Record Updated...");
			} else {
				System.out.println("Record Updation Failed!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}

}
