package day02;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class Demo01 {
public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		
		System.out.print("Enter Employee ID: ");
		int empId = new Scanner(System.in).nextInt();
		
		String selectQuery = "select * from employee where empId = " + empId;
		
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectQuery);
			
			if (resultSet.next()) {
				System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
				System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
				System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
			} else {
				System.out.println("Unable to Fetch the Employe Record!!!");
			}
			
			resultSet.close();
			statement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				if (connection != null) {					
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	

}
