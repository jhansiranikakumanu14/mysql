package day01;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class Demo03 {
public static void main(String[] args) {
		
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmployeeId and New Salary");
		int empId = scanner.nextInt();
		double salary = scanner.nextDouble();
		
		String updateQuery = "update employee set salary = " + salary + " where empId = " + empId;
				
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(updateQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) Updated...");
			} else {
				System.out.println("Record Updation Failed...");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
