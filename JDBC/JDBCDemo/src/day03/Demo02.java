package day03;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;

//Fetch All Employees

public class Demo02 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String selectQuery = "Select * from Employee";
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet != null) {
				while (resultSet.next()) {
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					System.out.println("\n");
				}
			} else {
				System.out.println("Unable to Fetch Employee Records");
			}
			
			resultSet.close();
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}


}
