package day03;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.db.DbConnection;

//Demonstrating ResultSetMetaData for Showcasing Column Names

public class Demo05 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		ResultSetMetaData resultSetMetaData = null;

		String selectQuery = "Select * from Employee";

		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			resultSet = preparedStatement.executeQuery();
			resultSetMetaData = resultSet.getMetaData();

			if (resultSet != null) {

				System.out.print("********************************************************************\n");

				System.out.print("* " + resultSetMetaData.getColumnName(1) + " ");
				System.out.print("* " + resultSetMetaData.getColumnName(2) + " ");
				System.out.print("* " + resultSetMetaData.getColumnName(3) + " ");
				System.out.print("* " + resultSetMetaData.getColumnName(4) + " ");
				System.out.print("* " + resultSetMetaData.getColumnName(5) + "          ");
				System.out.print("* " + resultSetMetaData.getColumnName(6) + " * \n");

				System.out.print("*******************************************************************\n");


				while (resultSet.next()) {
					System.out.print("* " + resultSet.getInt(1)    + "   ");
					System.out.print("* " + resultSet.getString(2) + "  ");
					System.out.print("* " + resultSet.getDouble(3) + "  ");
					System.out.print("* " + resultSet.getString(4) + " ");
					System.out.print("* " + resultSet.getString(5) + " ");
					System.out.print("* " + resultSet.getString(6) + "      * ");
					System.out.print("\n");

				}
				System.out.print("*******************************************************************\n");

			} else {
				System.out.println("Unable to Fetch Employee Record");
			}

			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
