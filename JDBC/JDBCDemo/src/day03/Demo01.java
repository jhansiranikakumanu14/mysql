package day03;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.db.DbConnection;
//Delete Employee Record
public class Demo01 {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		
		String deleteQuery = "Delete from Employee where empId = ?";
		
		System.out.print("Enter Employee ID: ");
		int empId = new java.util.Scanner(System.in).nextInt();
		
		try {
			preparedStatement = connection.prepareStatement(deleteQuery);
			preparedStatement.setInt(1, empId);
			int result = preparedStatement.executeUpdate();
			
			if (result > 0) {
				System.out.println("Employee with empId: " + empId + " Deleted Successfully!!!");
			} else {
				System.out.println("Failed to Delete the Employee Record!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
