package assignment;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class Assignment3 {
	public static void main(String[] args) {

        Connection connection = DbConnection.getConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        Scanner scanner = new Scanner(System.in);
        String allEmpIds = "SELECT empId FROM employee";
        System.out.println("Enter EmployeeId to get records: ");
        int requiredEmpId = scanner.nextInt();


        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(allEmpIds);

            if (resultSet != null) {
                System.out.println("Available Employee IDs:");
                while (resultSet.next()) {
                    System.out.println(resultSet.getInt(1));
                }
                System.out.println();
            } else {
                System.out.println("No Employee IDs Found!!!");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


        try {
            String selectQuery = "SELECT * FROM employee WHERE empId = " + requiredEmpId;
            resultSet = statement.executeQuery(selectQuery);

            if (resultSet != null) {
                while (resultSet.next()) {
                    System.out.println("EmpId   : " + resultSet.getInt(1));
                    System.out.println("EmpName : " + resultSet.getString(2));
                    System.out.println("Salary  : " + resultSet.getDouble("salary"));
                    System.out.println("Gender  : " + resultSet.getString(4));
                    System.out.println("EmailId : " + resultSet.getString(5));
                    System.out.println("Password: " + resultSet.getString(6));
                    System.out.println();
                }
            } else {
                System.out.println("No Record(s) Found for Employee ID: " + requiredEmpId);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            if (connection != null) {
                resultSet.close();
                statement.close();
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
