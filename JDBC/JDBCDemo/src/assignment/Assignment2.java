package assignment;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class Assignment2 {
	public static void main(String[] args) {

        Connection connection = DbConnection.getConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter EmployeeId to get records: ");
        int empId = scanner.nextInt();

        String selectQuery = "SELECT * FROM employee WHERE empId = " + empId;

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectQuery);

            if (resultSet != null) {
                while (resultSet.next()) {
                    System.out.println("EmpId   : " + resultSet.getInt(1));
                    System.out.println("EmpName : " + resultSet.getString(2));
                    System.out.println("Salary  : " + resultSet.getDouble("salary"));
                    System.out.println("Gender  : " + resultSet.getString(4));
                    System.out.println("EmailId : " + resultSet.getString(5));
                    System.out.println("Password: " + resultSet.getString(6));
                    System.out.println();
                }
            } else {
                System.out.println("No Record(s) Found!!!");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            if (connection != null) {
                resultSet.close();
                statement.close();
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
