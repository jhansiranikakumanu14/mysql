package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.db.DbConnection;
import com.dto.Employee;


public class EmployeeDao {
	 
	public Employee empLogin(String emailId, String password) {
		

		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String loginQuery = "Select * from employee where emailId=? and password=?";
		
		try {
			preparedStatement = connection.prepareStatement(loginQuery);
			preparedStatement.setString(1, emailId);
			preparedStatement.setString(2, password);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next()) {
				
				Employee employee = new Employee();
				
				employee.setEmpId(resultSet.getInt(1));
				employee.setEmpName(resultSet.getString(2));
				employee.setSalary(resultSet.getDouble(3));
				employee.setGender(resultSet.getString(4));
				employee.setEmailId(resultSet.getString(5));
				employee.setPassword(resultSet.getString(6));
				
				return employee;
				
			} 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					resultSet.close();
					preparedStatement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		
		return null;
	}
	
	
public int registerEmployee(Employee employee) {
		
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		
		String registerQuery = "insert into employee values (?, ?, ?, ?, ?, ?)";
		
		try {
			preparedStatement = connection.prepareStatement(registerQuery);
			
			preparedStatement.setInt(1, employee.getEmpId());
			preparedStatement.setString(2, employee.getEmpName());
			preparedStatement.setDouble(3, employee.getSalary());
			preparedStatement.setString(4, employee.getGender());
			preparedStatement.setString(5, employee.getEmailId());
			preparedStatement.setString(6, employee.getPassword());
			
			return preparedStatement.executeUpdate();			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					preparedStatement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return 0;
	}


public ArrayList<Employee> getAllEmployees() {
	Connection connection = DbConnection.getConnection();
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	
	String loginQuery = "Select * from employee";
	ArrayList<Employee> employeeList = new ArrayList<Employee>();
	
	try {
		preparedStatement = connection.prepareStatement(loginQuery);
		resultSet = preparedStatement.executeQuery();
		
		if (resultSet != null) {
			
			while (resultSet.next()) {
				
				Employee employee = new Employee();
				
				employee.setEmpId(resultSet.getInt(1));
				employee.setEmpName(resultSet.getString(2));
				employee.setSalary(resultSet.getDouble(3));
				employee.setGender(resultSet.getString(4));
				employee.setEmailId(resultSet.getString(5));
				employee.setPassword(resultSet.getString(6));
				
				employeeList.add(employee);
			}
			
			return employeeList;
		} 
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		try {
			if (connection != null) {
				resultSet.close();
				preparedStatement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}		
	
	return null;
}

public Employee getEmployeeById(int empId) {
	Connection connection = DbConnection.getConnection();
	PreparedStatement preparedStatement = null;
	ResultSet resultSet = null;
	
	String loginQuery = "Select * from employee where empId=?";
	
	try {
		preparedStatement = connection.prepareStatement(loginQuery);
		preparedStatement.setInt(1, empId);
		resultSet = preparedStatement.executeQuery();
		
		if (resultSet.next()) {
			
			Employee employee = new Employee();
			
			employee.setEmpId(resultSet.getInt(1));
			employee.setEmpName(resultSet.getString(2));
			employee.setSalary(resultSet.getDouble(3));
			employee.setGender(resultSet.getString(4));
			employee.setEmailId(resultSet.getString(5));
			employee.setPassword(resultSet.getString(6));
			
			return employee;
			
		} 
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		try {
			if (connection != null) {
				resultSet.close();
				preparedStatement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	return null;
}	
//public Employee getEmployeeByName(long empName) {
//	Connection connection = DbConnection.getConnection();
//	PreparedStatement preparedStatement = null;
//	ResultSet resultSet = null;
//	
//	String loginQuery = "Select * from employee where empName=?";
//	
//	try {
//		preparedStatement = connection.prepareStatement(loginQuery);
//		preparedStatement.setLong(1, empName);
//		resultSet = preparedStatement.executeQuery();
//		
//		if (resultSet.next()) {
//			
//			Employee employee = new Employee();
//			
//			employee.setEmpId(resultSet.getInt(1));
//			employee.setEmpName(resultSet.getString(2));
//			employee.setSalary(resultSet.getDouble(3));
//			employee.setGender(resultSet.getString(4));
//			employee.setEmailId(resultSet.getString(5));
//			employee.setPassword(resultSet.getString(6));
//			
//			return employee;
//			
//		} 
//		
//	} catch (SQLException e) {
//		e.printStackTrace();
//	}
//	
//	finally {
//		try {
//			if (connection != null) {
//				resultSet.close();
//				preparedStatement.close();
//				connection.close();
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
//	
//	return null;
//}	


}