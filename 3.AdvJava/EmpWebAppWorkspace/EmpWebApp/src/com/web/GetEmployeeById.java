package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;


@WebServlet("/GetEmployeeById")
public class GetEmployeeById extends HttpServlet {
	




	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int empId = Integer.parseInt(request.getParameter("empId"));
		EmployeeDao employeeDao = new EmployeeDao();
		Employee employee = employeeDao.getEmployeeById(empId);
			
		out.print("<body bgcolor='lightyellow' text='green'>");
		
		if (employee != null) {
			//adding the employee object under request object
			request.setAttribute("employee", employee);
				
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("GetEmployeeById.jsp");
			requestDispatcher.forward(request, response);
				
		}
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("HRHomePage.jsp");
		requestDispatcher.include(request, response);
		
		if (employee != null) {			
				
			out.print("<table border='2' align='center>");
				
			out.print("<tr>");
			out.print("<th> EmpId    </th>");
			out.print("<th> EmpName  </th>");
			out.print("<th> Salary   </th>");
			out.print("<th> Gender   </th>");
			out.print("<th> Email-Id </th>");
			out.print("<th> Password </th>");
			out.print("</tr>");

			out.print("<tr>");
			out.print("<td>" + employee.getEmpId()    + "</td>");
			out.print("<td>" + employee.getEmpName()  + "</td>");
			out.print("<td>" + employee.getSalary()   + "</td>");
			out.print("<td>" + employee.getGender()   + "</td>");
			out.print("<td>" + employee.getEmailId()  + "</td>");
			out.print("<td>" + employee.getPassword() + "</td>");
			out.print("</tr>");				
					
			out.print("</table>");
		} else {
			out.print("<h1 style='color:red'>Unable to Fetch Employee Records</h1>");
		}
		out.print("</body>");
	}
		
	

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request,response);
	}
//	public GetEmployeeById() {
//		         super();
//		
		    
		
}

