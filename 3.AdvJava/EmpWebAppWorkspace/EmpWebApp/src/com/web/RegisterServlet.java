package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dto.Employee;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int empId = Integer.parseInt(request.getParameter("empId"));
		String empName = request.getParameter("empName");
		double salary = Double.parseDouble(request.getParameter("salary"));
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		Employee employee = new Employee(empId, empName, salary, gender, emailId, password);
		
		EmployeeDao employeeDao = new EmployeeDao();
		int result = employeeDao.registerEmployee(employee);
		
		out.print("<body bgcolor='lightyellow' text='green'>");
		out.print("<center>");
		if (result > 0) {
			out.print("<h1>Employee Registration Success</h1>");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("Login.html");
			requestDispatcher.include(request, response);
		} else {
			out.print("<h1 style='color:red'>Employee Registration Failed...</h1>");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("Register.html");
			requestDispatcher.include(request, response);
		}

		out.print("</center></body>");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
