package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDao;
import com.dao.List;
import com.dto.Employee;


@WebServlet("/GetAllEmployees")
public class GetAllemployees extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		EmployeeDao employeeDao = new EmployeeDao();
		ArrayList<Employee> employeeList = employeeDao.getAllEmployees();
		
		out.print("<body bgcolor='lightyellow' text='green'>");
		
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("HRHomePage.jsp");
		requestDispatcher.include(request, response);
		
		if (employeeList != null) {			
			
			out.print("<table border='2'>");
			
			out.print("<tr>");
			out.print("<th> EmpId    </th>");
			out.print("<th> EmpName  </th>");
			out.print("<th> Salary   </th>");
			out.print("<th> Gender   </th>");
			out.print("<th> Email-Id </th>");
			out.print("<th> Password </th>");
			out.print("</tr>");
			
			
			for (Employee employee : employeeList) {
				
				out.print("<tr>");
				out.print("<td>" + employee.getEmpId()    + "</td>");
				out.print("<td>" + employee.getEmpName()  + "</td>");
				out.print("<td>" + employee.getSalary()   + "</td>");
				out.print("<td>" + employee.getGender()   + "</td>");
				out.print("<td>" + employee.getEmailId()  + "</td>");
				out.print("<td>" + employee.getPassword() + "</td>");
				out.print("</tr>");				
			}
						
			out.print("</table>");
			
		} else {
			out.print("<h1 style='color:red'>Unable to Fetch Employee Records</h1>");
		}
		out.print("</body>");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
