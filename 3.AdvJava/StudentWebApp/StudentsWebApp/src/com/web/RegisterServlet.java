package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		int studentId = Integer.parseInt(request.getParameter("studentId"));
		String name = request.getParameter("studentName");
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		String course = request.getParameter("course");
		double fee = Double.parseDouble(request.getParameter("fee"));
		
		
		PrintWriter out = response.getWriter();
		
		out.print("<html>");
		out.print("<body>");
		out.print("<center>");
		out.print("<h1>Student ID : "+ studentId +"</h1>");
		out.print("<h1>Student Name : "+ name +"</h1>");
		out.print("<h1>Student Gender : "+ gender +"</h1>");
		out.print("<h1>Student Email ID : "+ emailId +"</h1>");
		out.print("<h1>Student Password : "+ password +"</h1>");
		out.print("<h1>Course Name: "+ course +"</h1>");
		out.print("<h1>Course fee : "+ fee +"</h1>");
		out.print("</center>");
		out.print("</body>");
		out.print("</html>");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}