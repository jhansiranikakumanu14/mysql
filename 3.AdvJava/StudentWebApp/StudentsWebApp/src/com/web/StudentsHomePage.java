package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/StudentHomePage")
public class StudentsHomePage extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		String emailId = request.getParameter("emailId") ;
		PrintWriter out = response.getWriter();
		
		out.print("<html>");
		out.print("<body>");
		out.print("<div align = 'right'>");
		out.print("<a href = 'StudentHomePage'>Home</a> &nbsp; | &nbsp;");
		out.print("<a href = 'Login.html'>LogOut</a>");
		out.print("</div>");
		out.print("<h2>Welcome to Student Home Page!!....."+ emailId +"</h2>");
		out.print("</body>");
		out.print("</html>");
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}